<?php

namespace App\Http\Controllers;

use App\Project;
use App\Services\Twitter;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index ()
    {
        $projects = Project ::all ();

        return view ( 'projects.index', compact ( 'projects' ) );
    }

    public function create ()
    {
        return view ( 'projects.create' );
    }

    public function show ( Project $project )
    {
        return view ( 'projects.show', compact ( 'project' ) );
    }

    public function edit ( Project $project )
    {
        return view ( 'projects.edit', compact ( 'project' ) );
    }

    public function update ( Project $project )
    {
        $project -> title       = \request ( 'title' );
        $project -> description = \request ( 'description' );

        $project -> save ();

        return redirect ( '/projects' );
    }

    public function destroy ( Project $project )
    {
        $project -> delete ();

        return redirect ( '/projects' );
    }

    public function store ()
    {
        $validated = request () -> validate (
            [
                'title'       => ['required', 'min:3'],
                'description' => 'required'
            ]
        );

        Project ::create ( $validated );

        return redirect ( '/projects' );
    }
}
