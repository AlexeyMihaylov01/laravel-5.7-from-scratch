<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use App\Task;

class ProjectTasksController extends Controller
{
    public function update ( Task $task )
    {
        if ( request () -> has ( 'completed' ) ) {
            $task -> complete ();
        } else {
            $task -> incomplete ();
        }

        return back ();
    }

    public function store ( Project $project )
    {
        $attributes = \request () -> validate ( [ 'description' => 'required', ] );

        $project -> addTask ( $attributes );

        return back ();
    }
}
